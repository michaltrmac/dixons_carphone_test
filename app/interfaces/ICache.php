<?php

namespace App\Interfaces;

/**
 * Description of ICache
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
interface ICache
{

	/**
	 *
	 * @param string $id
	 * @return array
	 */
	public function findById(string $id): array;

	/**
	 *
	 * @param string $id
	 * @param array $data
	 * @return bool
	 */
	public function save(string $id, array $data): bool;

}
