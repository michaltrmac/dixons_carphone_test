<?php

namespace App\Interfaces;

/**
 * Description of IMySQLDriver
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
interface IMySQLDriver
{
	/**
	 * @param string $id
	 * @return array
	 */
	public function findProduct(string $id): array;
}