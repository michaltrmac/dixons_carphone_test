<?php

namespace App\Interfaces;

/**
 * Description of IElasticSearchDriver
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
interface IElasticSearchDriver
{
	/**
	 * @param string $id
	 * @return array
	 */
	public function findById(string $id): array;
}