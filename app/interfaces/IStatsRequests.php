<?php

namespace App\Interfaces;

/**
 * Description of IRequests
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
interface IStatsRequests
{

	/**
	 *
	 * @param string $id
	 * @param int $inc
	 */
	public function incById(string $id, int $inc = 1);

	/**
	 *
	 * @param string $id
	 * @return int
	 */
	public function getById(string $id): int;

}
