<?php

namespace App\Interfaces;

/**
 * Description of IFileStorage
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
interface IFileStorage
{

	/**
	 *
	 * @return string
	 */
	public function getStorageBaseDir(): string;

	/**
	 *
	 * @param string $filename
	 * @param mixed $data
	 * @return string
	 */
	public function storeFile(string $filename, $data): string;

	/**
	 *
	 * @param string $filename
	 * @return mixed
	 */
	public function readFile(string $filename);

}
