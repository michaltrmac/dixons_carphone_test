<?php

namespace App\Exception;

/**
 * Description of CacheNotFound
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
class CacheNotFound extends \Exception
{

	/** @var string */
	protected $message = 'Cache with key "%key%" not found.';

	/**
	 *
	 * @param string $key
	 * @param int $code
	 * @param \Throwable $previous
	 */
	public function __construct(string $key, int $code = 0, \Throwable $previous = null)
	{
		parent::__construct(str_replace('%key%', $key, $this->message), $code, $previous);
	}

}
