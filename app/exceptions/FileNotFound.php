<?php

namespace App\Exception;

/**
 * Description of FileNotFound
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
class FileNotFound extends \Exception
{

	/** @var string */
	protected $message = 'File "%file%" not found.';

	/**
	 *
	 * @param string $file
	 * @param int $code
	 * @param \Throwable $previous
	 */
	public function __construct(string $file, int $code = 0, \Throwable $previous = null)
	{
		parent::__construct(str_replace('%file%', $file, $this->message), $code, $previous);
	}

}
