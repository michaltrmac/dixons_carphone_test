<?php

namespace App\Controllers;

/**
 * Description of ProductController
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
class ProductController
{

	/** @var bool */
	private $use_elastic;

	/** @var \App\Interfaces\ICache */
	private $cache;

	/** @var \App\Models\Product\Mysql */
	private $product_mysql;

	/** @var \App\Models\Product\ElasticSearch */
	private $product_es;

	/** @var \App\Interfaces\IStatsRequests */
	private $stats_requests;

	/**
	 *
	 * @param bool $use_elastic
	 * @param \App\Interfaces\ICache $cache
	 * @param \App\Models\Product\Mysql $product_mysql
	 * @param \App\Models\Product\ElasticSearch $product_es
	 * @param \App\Interfaces\IStatsRequests $stats_requests
	 */
	public function __construct(bool $use_elastic, \App\Interfaces\ICache $cache, \App\Models\Product\Mysql $product_mysql,
			\App\Models\Product\ElasticSearch $product_es, \App\Interfaces\IStatsRequests $stats_requests)
	{
		$this->use_elastic = (bool)$use_elastic;
		$this->cahce = $cache;
		$this->product_mysql = $product_mysql;
		$this->product_es = $product_es;
		$this->stats_requests = $stats_requests;
	}

	/**
	 * @param string $id
	 * @return string
	 */
	public function detail(string $id): string
	{
		try {
			$product_data = $this->cache->findById($id);
		} catch (\App\Exception\CacheNotFound $e) {
			if ($this->use_elastic) {
				$product_data = $this->product_es->findById($id);
			} else {
				$product_data = $this->product_mysql->findProduct($id);
			}

			$this->cache->save($id, $product_data);
		}

		$this->stats_requests->incById($id);

		return json_encode($product_data);
	}

}