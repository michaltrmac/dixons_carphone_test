<?php

namespace App\Models\Product;

/**
 * Description of ElasticSearch
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
class ElasticSearch implements \App\Interfaces\IElasticSearchDriver
{

	/**
	 *
	 * @param string $id
	 * @return array
	 */
	public function findById(string $id): array
	{
		// the body of function may look like this
		return $this->index('products')
				->getById('id', $id);
	}

}
