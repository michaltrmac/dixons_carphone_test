<?php

namespace App\Models\Product\Stats;

/**
 * Description of File
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
class File implements \App\Interfaces\IFileStorage, \App\Interfaces\IStatsRequests
{

	/** @var string */
	private $storage_base_dir;

	/**
	 *
	 * @param string $storage_base_dir
	 */
	public function __construct(string $storage_base_dir)
	{
		$this->storage_base_dir = $storage_base_dir;
	}

	/**
	 *
	 * @param string $id
	 * @return int
	 */
	public function getById(string $id): int
	{
		try {
			return $this->readFile($id);
		}
		catch (App\Exception\FileNotFound $e) {
			return 0;
		}
	}

	/**
	 *
	 * @return string
	 */
	public function getStorageBaseDir(): string
	{
		// should be set in config
		return $this->storage_base_dir;
	}

	/**
	 *
	 * @param string $id
	 * @param int $inc
	 * @return int
	 */
	public function incById(string $id, int $inc = 1): int
	{
		try {
			$count = $this->readFile($id);
		} catch (\App\Exception\FileNotFound $e) {
			$count = 0;
		}

		$count += (int)$inc;

		$this->storeFile($id, $count);

		return $count;
	}

	/**
	 *
	 * @param string $filename
	 * @return int
	 */
	public function readFile(string $filename)
	{
		$file = $this->getStorageBaseDir.DIRECTORY_SEPARATOR.md5($filename);
		if (!file_exists($file) || !is_readable($file))
			throw new \App\Exception\FileNotFound($file);

		return (int)file_get_contents($file);
	}

	/**
	 *
	 * @param string $filename
	 * @param int $count
	 * @return string
	 */
	public function storeFile(string $filename, $count): string
	{
		$file = $this->getStorageBaseDir.DIRECTORY_SEPARATOR.md5($filename);
		$fp = fopen($file, 'w');
		fwrite($fp, $count);
		fclose($fp);

		return $file;
	}

}
