<?php

namespace App\Models\Product;

/**
 * Description of Mysql
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
class Mysql implements \App\Interfaces\IMySQLDriver
{

	/**
	 *
	 * @param string $id
	 * @return array
	 */
	public function findProduct(string $id): array
	{
		// the body of function may look like this
		return $this->db->table('products')
				->where('id = ?', $id)
				->fetch();
	}

}
