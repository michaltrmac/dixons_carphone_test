<?php

namespace App\Models\Cache;

/**
 * Description of File
 *
 * @author Michal Trmač <michal.trmac@gmail.com>
 */
class File implements \App\Interfaces\ICache, \App\Interfaces\IFileStorage
{

	/** @var string */
	private $storage_base_dir;

	/**
	 *
	 * @param string $storage_base_dir
	 */
	public function __construct(string $storage_base_dir)
	{
		$this->storage_base_dir = $storage_base_dir;
	}

	/**
	 *
	 * @param string $id
	 * @return array
	 * @throws \App\Exception\CacheNotFound
	 */
	public function findById(string $id): array
	{
		try {
			return $this->readFile($id);
		}
		catch (App\Exception\FileNotFound $e) {
			throw new \App\Exception\CacheNotFound($id);
		}
	}

	/**
	 *
	 * @param string $id
	 * @param array $data
	 * @return bool
	 */
	public function save(string $id, array $data): bool
	{
		$new_file = $this->storeFile($id, $data);
		return file_exists($new_file) && is_readable($new_file);
	}

	/**
	 *
	 * @return string
	 */
	public function getStorageBaseDir(): string
	{
		// should be set in config
		return $this->storage_base_dir;
	}

	/**
	 *
	 * @param string $filename
	 * @return mixed
	 */
	public function readFile(string $filename)
	{
		$file = $this->getStorageBaseDir.DIRECTORY_SEPARATOR.md5($filename);
		if (!file_exists($file) || !is_readable($file))
			throw new \App\Exception\FileNotFound($file);

		$data = file_get_contents($file);

		return unserialize($data);
	}

	/**
	 *
	 * @param string $filename
	 * @param mixed $data
	 * @return string
	 */
	public function storeFile(string $filename, $data): string
	{
		$new_file = $this->getStorageBaseDir.DIRECTORY_SEPARATOR.md5($filename);
		$fp = fopen($new_file, 'w');
		fwrite($fp, serialize($data));
		fclose($fp);

		return $new_file;
	}

}
